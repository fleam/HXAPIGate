/**
 * Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package hx.apigate.distributedCache.config;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;


/**
 * 
 * <p>Description: 分布式缓存</p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: www.uiotcp.com</p>
 * @author yangcheng
 * @date 2019年11月12日
 * @version 1.0
 */
public class DistributedCachingProperties implements Serializable{
	/**
	 * single or cluster
	 */
	private String defualtCacheProcessorName = "single";  
	/**
	 * 取值为：fastJsonSerialize或protobufSerialize
	 */
	private String BASIC_SERIALIZATION = "fastJsonSerialize";
	
	//单节点
	private RedisNodes redis_host = new RedisNodes("127.0.0.1",16379);//节点
	private String redis_password = "OFF";
	private int redis_timeout = 10000;
	private int redis_maxIdle =  299;
	private int redis_maxTotal = 1000;
	private int redis_maxWaitMillis = 1000;
	private int redis_minEvictableIdleTimeMillis  = 300000;
	private boolean redis_testOnBorrow = true;
	private boolean redis_testOnReturn = true;
	
	//集群节点
	private List<RedisNodes> redis_cluster_host = Arrays.asList(new RedisNodes("127.0.0.1",6379));//节点
	private String redis_cluster_password = "root";
	private int redis_cluster_timeout = 10000;
	private int redis_cluster_maxIdle  = 299;
	private int redis_cluster_maxTotal = 1000;
	private int redis_cluster_maxWaitMillis = 1000;
	private int redis_cluster_minEvictableIdleTimeMillis = 300000;
	private int redis_cluster_connectionTimeout = 5000;//连接超时时间
	private int redis_cluster_soTimeout = 5000;
	private int redis_cluster_maxAttempts = 10;//连接最大重试次数
	private boolean redis_cluster_testOnBorrow = true;
	private boolean redis_cluster_testOnReturn = true;
	
	
	public RedisNodes getRedis_host() {
		return redis_host;
	}

	public String getRedis_password() {
		return redis_password;
	}

	public int getRedis_timeout() {
		return redis_timeout;
	}

	public int getRedis_maxIdle() {
		return redis_maxIdle;
	}

	public int getRedis_maxTotal() {
		return redis_maxTotal;
	}

	public int getRedis_maxWaitMillis() {
		return redis_maxWaitMillis;
	}

	public int getRedis_minEvictableIdleTimeMillis() {
		return redis_minEvictableIdleTimeMillis;
	}

	public boolean isRedis_testOnBorrow() {
		return redis_testOnBorrow;
	}

	public boolean isRedis_testOnReturn() {
		return redis_testOnReturn;
	}

	public String getDefualtCacheProcessorName() {
		return defualtCacheProcessorName;
	}

	public List<RedisNodes> getRedis_cluster_host() {
		return redis_cluster_host;
	}


	public String getRedis_cluster_password() {
		return redis_cluster_password;
	}

	public int getRedis_cluster_timeout() {
		return redis_cluster_timeout;
	}

	public int getRedis_cluster_maxIdle() {
		return redis_cluster_maxIdle;
	}

	public int getRedis_cluster_maxTotal() {
		return redis_cluster_maxTotal;
	}

	public int getRedis_cluster_maxWaitMillis() {
		return redis_cluster_maxWaitMillis;
	}

	public int getRedis_cluster_minEvictableIdleTimeMillis() {
		return redis_cluster_minEvictableIdleTimeMillis;
	}

	public boolean isRedis_cluster_testOnBorrow() {
		return redis_cluster_testOnBorrow;
	}


	public boolean isRedis_cluster_testOnReturn() {
		return redis_cluster_testOnReturn;
	}

	public int getRedis_cluster_connectionTimeout() {
		return redis_cluster_connectionTimeout;
	}

	public int getRedis_cluster_soTimeout() {
		return redis_cluster_soTimeout;
	}

	public int getRedis_cluster_maxAttempts() {
		return redis_cluster_maxAttempts;
	}

	public String getBASIC_SERIALIZATION() {
		return BASIC_SERIALIZATION;
	}

	public void setBASIC_SERIALIZATION(String bASIC_SERIALIZATION) {
		BASIC_SERIALIZATION = bASIC_SERIALIZATION;
	}

	public static class RedisNodes implements Serializable{

	    private String ip;
	    private int port;
	    
	    public RedisNodes() {
			super();
		}
		public RedisNodes(String ip, int port) {
			super();
			this.ip = ip;
			this.port = port;
		}
		public String getIp() {
			return ip;
		}
		public void setIp(String ip) {
			this.ip = ip;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}

		@Override
	    public String toString() {
	      return "redisNodes{" +
	          "ip='" + ip + '\'' +
	          ", port=" + port +
	          '}';
	    }
	}
	
}
