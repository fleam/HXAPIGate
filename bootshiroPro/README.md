### bootshiroPro简介

bootshiroPro是基于bootshiro开源项目（地址： https://gitee.com/tomsun28/bootshiro ）定制而来，结合HXAPIGate做了定制更改，因此如果您并不打算使用HXAPIGate的话，那么我建议您使用bootshiro项目开发，因为后续我做的所有对bootshiroPro的优化都是针对HXAPIGate搭配使用而定制的，不具有通用性！